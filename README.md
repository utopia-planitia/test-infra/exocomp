# Exocomp Bot

Exocomp bot runs scripted code-changes (creates merge requests), issue creation and repository scans.

## Environment Variables

Set the environment variable EXOCOMP_DISABLE_PUSH to a non-empty value to prevent exocomp from pushing changes.

## Examples

```sh
go run cmd/main.go \
    --limit-to-project=1724 \
    code \
    --disable-automerge=true \
    --disable-push=true \
    find-and-replace \
    --find-string=git.votum-media.net \
    --replace-with=git.turbinekreuzberg.io \
    --branch-name='exocomp/AM-657-update-gitlab-domain' \
    --merge-request-title="AM-657: update GitLab domain" \
    --merge-request-description="$(printf 'Replace the deprecated domain `git.votum-media.net` with `git.turbinekreuzberg.io`.\n\n<small>This merge request has been created by a bot. If you have any questions, please approach us in the Slack channel #team-platform.</small>\n')"
```
