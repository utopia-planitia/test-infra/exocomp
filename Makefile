UID = $(shell id -u)
GID = $(shell id -g)

# MAKECMDGOALS: is a special variable that lists the goals you specified on the command line
#               for example with `make run tasks` the MAKECMDGOALS will be `run tasks`
# $(filter-out $@,$(MAKECMDGOALS)): returns MAKECMDGOALS without the name of the current target
run: image
	docker run -ti --rm \
		-v "${PWD}":/exocomp \
		-v tmp:/tmp \
		-w /exocomp \
		--env GOMODCACHE=/tmp/go/pkg/mod \
		--env XDG_CACHE_HOME=/tmp/cache \
		--user "$(UID):$(GID)" \
		exocomp-image \
		go run cmd/main.go $(filter-out $@,$(MAKECMDGOALS))

# \
		-e EXOCOMP_PROJECT_ID=18281468\
		-e EXOCOMP_DISABLE_PUSH=1 \
# 15559602 hetznerctl	(go + test service)
# 4584087  registry		(multiple services)
# 18281468 exocomp		(only go)
# 8241924  integration cluster
# 26589259 integration pipeline
# 25707447 renovate bot
# 4594360  ingress
# 4593430  monitoring
# 4894346  gitlab-runner

image:
	docker build -t exocomp-image .

rebuild-image:
	docker build --no-cache -t exocomp-image .

lint:
	docker run -ti --rm -w ${PWD} -v ${PWD}:${PWD} golangci/golangci-lint:v1.54.1-alpine golangci-lint --timeout=180s --skip-dirs="(^|/)data($$|/)" run ./...

.PHONY: test
test: image
	docker run -ti --rm \
		-v "${PWD}":/exocomp \
		-v tmp:/tmp \
		-w /exocomp \
		--env GOMODCACHE=/tmp/go/pkg/mod \
		--env XDG_CACHE_HOME=/tmp/cache \
		--user "$(UID):$(GID)" \
		exocomp-image \
		go test ./...

# match ("%:") all CLI commands / flags / options / etc. that did not match any other make goals / targets and ignore them ("@:")
%:
	@:
