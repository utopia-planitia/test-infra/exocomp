package find

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func Test_executeGrep(t *testing.T) {
	type args struct {
		pattern     string
		patternType PatternType
	}
	type file struct {
		path    string
		name    string
		content string
	}
	type test struct {
		name               string
		files              []file
		args               args
		expectStdoutStderr func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool
		wantErr            bool
	}

	tests := []test{
		{
			name: "check if we find something",
			files: []file{
				{
					path:    "./dir1",
					name:    "test.txt",
					content: "baz\nfoo",
				},
			},
			args: args{
				pattern:     "foo",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "./dir1/test.txt:2:foo\n" && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check if we find something with a BasicRegExp pattern",
			files: []file{
				{
					path:    "./dir1",
					name:    "test.txt",
					content: "abc\nd\n123\n",
				},
			},
			args: args{
				pattern:     "[a-z][a-z]\\+",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "./dir1/test.txt:1:abc\n" && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check if we find something with a FixedString pattern",
			files: []file{
				{
					path:    "./dir1",
					name:    "test.txt",
					content: "abc\nd\n123\n",
				},
			},
			args: args{
				pattern:     "abc",
				patternType: PatternTypeFixedStrings,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "./dir1/test.txt:1:abc\n" && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check if we find something with an ExtendedRegExp pattern",
			files: []file{
				{
					path:    "./dir1",
					name:    "test.txt",
					content: "abc\nd\n123\n",
				},
			},
			args: args{
				pattern:     "^[a-z]{3,}",
				patternType: PatternTypeExtendedRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "./dir1/test.txt:1:abc\n" && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check if we find something with an PerlRegExp pattern",
			files: []file{
				{
					path:    "./dir1",
					name:    "test.txt",
					content: "abc\n123\n",
				},
			},
			args: args{
				pattern:     "^[\\D]{3,}",
				patternType: PatternTypePerlRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "./dir1/test.txt:1:abc\n" && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check we find nothing",
			files: []file{
				{
					path:    "./dir1/dir2",
					name:    "test.txt",
					content: "foo",
				},
			},
			args: args{
				pattern:     "bar",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "" && stderr.String() == ""
			},
			wantErr: true,
		},
		{
			name: "check we find two matching files",
			files: []file{
				{
					path:    ".",
					name:    "test1.txt",
					content: "foo",
				},
				{
					path:    "./dir1",
					name:    "test2.txt",
					content: "foo",
				},
			},
			args: args{
				pattern:     "foo",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				expectedStdoutMatches :=
					strings.Contains(stdout.String(), "./dir1/test2.txt:1:foo") &&
						strings.Contains(stdout.String(), "./test1.txt:1:foo")

				return expectedStdoutMatches && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check two files but only one matches",
			files: []file{
				{
					path:    ".",
					name:    "test1.txt",
					content: "foo",
				},
				{
					path:    "./dir1",
					name:    "test2.txt",
					content: "bar",
				},
			},
			args: args{
				pattern:     "foo",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				return stdout.String() == "./test1.txt:1:foo\n" && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check for pattern in two files",
			files: []file{
				{
					path:    ".",
					name:    "test1.txt",
					content: "aoo",
				},
				{
					path:    "./dir1",
					name:    "test2.txt",
					content: "boo",
				},
			},
			args: args{
				pattern:     "[a-b]oo",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				expectedStdoutMatches :=
					strings.Contains(stdout.String(), "./dir1/test2.txt:1:boo") &&
						strings.Contains(stdout.String(), "./test1.txt:1:aoo")

				return expectedStdoutMatches && stderr.String() == ""
			},
			wantErr: false,
		},
		{
			name: "check multiple patterns in files",
			files: []file{
				{
					path:    ".",
					name:    "test1.txt",
					content: "aoo",
				},
				{
					path:    "./dir1",
					name:    "test2.txt",
					content: "foo",
				},
			},
			args: args{
				pattern:     "aoo\\|foo",
				patternType: PatternTypeBasicRegexp,
			},
			expectStdoutStderr: func(stdout *bytes.Buffer, stderr *bytes.Buffer) bool {
				expectedStdoutMatches :=
					strings.Contains(stdout.String(), "./dir1/test2.txt:1:foo") &&
						strings.Contains(stdout.String(), "./test1.txt:1:aoo")

				return expectedStdoutMatches && stderr.String() == ""
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var stdout bytes.Buffer
			var stderr bytes.Buffer

			dir := t.TempDir()

			for _, file := range tt.files {
				filePath := "."
				if file.path != "" {
					filePath = file.path
				}

				folderPath := filepath.Join(dir, filePath)

				err := os.MkdirAll(folderPath, os.ModePerm)
				if err != nil {
					t.Errorf("can't create directory '%s'", folderPath)
				}

				filename := "test.txt"
				if file.name != "" {
					filename = file.name
				}

				err = os.WriteFile(filepath.Join(folderPath, filename), []byte(file.content), 0666)
				if err != nil {
					t.Errorf("can't create  file '%s'", filepath.Join(folderPath, filename))
				}
			}

			err := executeGrep(tt.args.pattern, tt.args.patternType, &stdout, &stderr, dir)
			if (err != nil) != tt.wantErr {
				t.Errorf("executeGrep() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.expectStdoutStderr(&stdout, &stderr) == false {
				t.Errorf("executeGrep() stdout is '%s'; stderr is '%s'", &stdout, &stderr)
			}
		})
	}
}
