package find

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"

	"al.essio.dev/pkg/shellescape"
	cli "github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

// ENUM(basic-regexp, extended-regexp, fixed-strings, perl-regexp)
type PatternType string

var _ exocomp.Scan = (*Scan)(nil) // Verify that *Scan implements exocomp.Scan.

type Scan struct {
	cli *cli.Context //todo scan doesn't need cli
}

func (t *Scan) New(c *cli.Context) exocomp.Task {
	return &Scan{cli: c}
}

func (t *Scan) Enabled() bool {
	return true
}

func (t *Scan) Batchable() bool {
	return true
}

func (t *Scan) Name() string {
	return "find"
}

func (t *Scan) Emoji() rune {
	return '🔎'
}

func (t *Scan) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t *Scan) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:     "pattern",
			Required: true,
		},
		&cli.StringFlag{
			Name:     "pattern-type",
			Value:    string(PatternTypeBasicRegexp),
			Usage:    fmt.Sprintf("supported pattern types: %s", strings.Join(PatternTypeNames(), ", ")),
			Required: false,
			Action: func(ctx *cli.Context, patternTypeString string) error {
				_, err := ParsePatternType(patternTypeString)
				if err != nil {
					return err
				}
				return nil
			},
		},
	}
}

// Source: https://github.com/openconfig/goyang/blob/v1.0.0/pkg/indent/indent.go#L51
// NewWriter returns an io.Writer that prefixes the lines written to it with
// indent and then writes them to w.  The writer returns the number of bytes
// written to the underlying Writer.
func newWriter(w io.Writer, indent string, prependLine string) io.Writer {
	var prefix []byte = nil
	if indent != "" {
		prefix = []byte(indent)
	}

	var prependLineWriter []byte = nil
	if prependLine != "" {
		prependLine += "\n"
		prependLineWriter = []byte(prependLine)
	}

	return &iw{
		w:           w,
		prefix:      prefix,
		prependLine: prependLineWriter,
	}
}

// Source: https://github.com/openconfig/goyang/blob/v1.0.0/pkg/indent/indent.go#L51
type iw struct {
	w           io.Writer
	prefix      []byte
	prependLine []byte
	partial     bool // true if next line's indent already written
}

// Source: https://github.com/openconfig/goyang/blob/v1.0.0/pkg/indent/indent.go#L51
// Write implements io.Writer.
func (w *iw) Write(buf []byte) (int, error) {
	if len(buf) == 0 {
		return 0, nil
	}
	lines := bytes.SplitAfter(buf, []byte{'\n'})
	lines = append([][]byte{w.prependLine}, lines...)
	if len(lines[len(lines)-1]) == 0 {
		lines = lines[:len(lines)-1]
	}
	if !w.partial {
		lines = append([][]byte{{}}, lines...)
	}
	joined := bytes.Join(lines, w.prefix)
	w.partial = joined[len(joined)-1] != '\n'

	n, err := w.w.Write(joined)
	if err != nil {
		return actualWrittenSize(n, len(w.prefix), lines), err
	}

	return len(buf), nil
}

// Source: https://github.com/openconfig/goyang/blob/v1.0.0/pkg/indent/indent.go#L51
func actualWrittenSize(underlay, prefix int, lines [][]byte) int {
	actual := 0
	remain := underlay
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}

		addition := remain - prefix
		if addition <= 0 {
			return actual
		}

		if addition <= len(line) {
			return actual + addition
		}

		actual += len(line)
		remain -= prefix + len(line)
	}

	return actual
}

func (t *Scan) Execute(r *exocomp.Repo, p *gitlab.Project, _ *gitlab.Group, _ *exocomp.Gitlab, c *cli.Context) error {

	pattern := c.String("pattern")
	patternType, err := ParsePatternType(c.String("pattern-type"))
	if err != nil {
		return err
	}

	log.Printf("%c executing \"%s\" Scan, searching for pattern \"%s\" in files", t.Emoji(), t.Name(), pattern)

	prependLine := "Found:"
	w := newWriter(os.Stdout, "    ", prependLine)

	err = executeGrep(pattern, patternType, w, os.Stderr, r.WorkingDir())

	// Normally 'Exit Code 1' for `grep` means nothing was found.
	// Exit codes other than 0 or 1 indicate that an error ocurred
	if exitError, ok := err.(*exec.ExitError); ok {
		if exitError.ExitCode() == 1 {
			// log.Printf("Nothing was found.\n")
			return nil
		}
	}

	return err
}

func executeGrep(pattern string, patternType PatternType, out io.Writer, errW io.Writer, workingDir string) error {

	cmd := grepCmd(pattern, patternType, out, errW, workingDir)

	err := cmd.Run()

	return err
}

func grepCmd(pattern string, patternType PatternType, stdOut io.Writer, stdErr io.Writer, workingDir string) *exec.Cmd {
	script := fmt.Sprintf("grep --exclude-dir=.git --recursive --line-number --%s %s .", shellescape.Quote(string(patternType)), shellescape.Quote(pattern))

	cmd := exec.Command("bash")
	cmd.Dir = workingDir
	cmd.Stdin = strings.NewReader(script)
	cmd.Stdout = stdOut
	cmd.Stderr = stdErr

	return cmd
}
