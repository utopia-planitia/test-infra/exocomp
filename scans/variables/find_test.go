package variables

import (
	"encoding/base64"
	"testing"
)

func Test_find(t *testing.T) {
	type args struct {
		needle   string
		haystack string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "expect true when the plain string haystack contains the needle",
			args: args{
				needle:   "test",
				haystack: "This is a test!",
			},
			want: true,
		},
		{
			name: "expect false when the plain string haystack does not contain the needle",
			args: args{
				needle:   "needle",
				haystack: "This is a test!",
			},
			want: false,
		},
		{
			name: "expect true when the base64 encoded haystack contains the needle",
			args: args{
				needle:   "test",
				haystack: base64.StdEncoding.EncodeToString([]byte("This is a test!")),
			},
			want: true,
		},
		{
			name: "expect false when the base64 encoded haystack does not contain the needle",
			args: args{
				needle:   "needle",
				haystack: base64.StdEncoding.EncodeToString([]byte("This is a test!")),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := find(tt.args.needle, tt.args.haystack); got != tt.want {
				t.Errorf("find() = %v, want %v", got, tt.want)
			}
		})
	}
}
