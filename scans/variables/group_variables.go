package variables

import (
	"errors"
	"fmt"

	cli "github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

// Verify that the exocomp.Scan interface is implemented.
// var _ exocomp.Scan = Scan{} // Use this line instead of the next line when this Scan is not a pointer type anymore.
var _ exocomp.Scan = (*GroupScan)(nil)

type GroupScan struct{}

func (t *GroupScan) New(c *cli.Context) exocomp.Task {
	return t
}

func (t *GroupScan) Enabled() bool {
	return true
}

func (t *GroupScan) Batchable() bool {
	return true
}

func (t *GroupScan) Name() string {
	return "group-variables"
}

func (t *GroupScan) Emoji() rune {
	return '🔎'
}

func (t *GroupScan) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeGroup
}

const groupValueFlagName = "value"

func (t *GroupScan) Flags() []cli.Flag {
	return []cli.Flag{
		// &cli.StringFlag{
		// 	Name:     "key",
		// 	Required: true,
		// },
		&cli.StringFlag{
			Name:     groupValueFlagName,
			Required: true,
		},
	}
}

// Execute is intended to execute against local repo, e.g. find, search,...
func (t *GroupScan) Execute(_ *exocomp.Repo, _ *gitlab.Project, group *gitlab.Group, g *exocomp.Gitlab, c *cli.Context) error {
	needle := c.String(groupValueFlagName)
	if needle == "" {
		return nil
	}

	if group == nil {
		return errors.New("expected *gitlab.Group pointer, but got nil")
	}
	if g == nil {
		return errors.New("expected *exocomp.Gitlab pointer, but got nil")
	}
	if c == nil {
		return errors.New("expected *cli.Context pointer, but got nil")
	}

	variables, err := g.LookupGroupVariables(group)
	if err != nil {
		return fmt.Errorf("lookup group-level variables: %v", err)
	}

	for _, variable := range variables {
		if find(needle, variable.Value) {
			fmt.Printf("  %s: found \"%s\" in value of %s\n", t.Name(), needle, variable.Key)
		}
	}

	return nil
}
