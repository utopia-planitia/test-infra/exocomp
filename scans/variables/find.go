package variables

import (
	"encoding/base64"
	"strings"
)

func find(needle, haystack string) bool {
	if strings.Contains(haystack, needle) {
		return true
	}

	// Some CI/CD variables might be base64 encoded. Try to decode the haystack string (best-effort) and if it succeeds, we check if the result contains the needle.
	b64, _ := base64.StdEncoding.DecodeString(haystack)
	if b64 != nil {
		return strings.Contains(string(b64), needle)
	}

	return false
}
