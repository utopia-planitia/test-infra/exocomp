package variables

import (
	"errors"
	"fmt"

	cli "github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

// Verify that the exocomp.Scan interface is implemented.
// var _ exocomp.Scan = Scan{} // Use this line instead of the next line when this Scan is not a pointer type anymore.
var _ exocomp.Scan = (*ProjectScan)(nil)

type ProjectScan struct{}

func (t *ProjectScan) New(c *cli.Context) exocomp.Task {
	return t
}

func (t *ProjectScan) Enabled() bool {
	return true
}

func (t *ProjectScan) Batchable() bool {
	return true
}

func (t *ProjectScan) Name() string {
	return "project-variables"
}

func (t *ProjectScan) Emoji() rune {
	return '🔎'
}

func (t *ProjectScan) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

const projectValueFlagName = "value"

func (t *ProjectScan) Flags() []cli.Flag {
	return []cli.Flag{
		// &cli.StringFlag{
		// 	Name:     "key",
		// 	Required: true,
		// },
		&cli.StringFlag{
			Name:     projectValueFlagName,
			Required: true,
		},
	}
}

// Execute is intended to execute against local repo, e.g. find, search,...
func (t *ProjectScan) Execute(_ *exocomp.Repo, p *gitlab.Project, _ *gitlab.Group, g *exocomp.Gitlab, c *cli.Context) error {
	needle := c.String(projectValueFlagName)
	if needle == "" {
		return nil
	}

	if p == nil {
		return errors.New("expected *gitlab.Project pointer, but got nil")
	}
	if g == nil {
		return errors.New("expected *exocomp.Gitlab pointer, but got nil")
	}
	if c == nil {
		return errors.New("expected *cli.Context pointer, but got nil")
	}

	variables, err := g.LookupProjectVariables(p)
	if err != nil {
		return fmt.Errorf("lookup group-level variables: %v", err)
	}

	for _, variable := range variables {
		if find(needle, variable.Value) {
			fmt.Printf("  %s: found \"%s\" in value of %s\n", t.Name(), needle, variable.Key)
		}
	}

	return nil
}
