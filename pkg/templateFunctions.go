package exocomp

import (
	"bytes"
	"fmt"
	"path/filepath"
	"text/template"
)

func RenderTemplate(tpl string, data interface{}) ([]byte, error) {
	var fns = template.FuncMap{
		"plus1": func(x int) int {
			return x + 1
		},
	}

	tmpl, err := template.New("template-name").Funcs(fns).Parse(tpl)
	if err != nil {
		return []byte{}, fmt.Errorf("parse template: %v", err)
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, data)
	if err != nil {
		return []byte{}, fmt.Errorf("render template: %v", err)
	}

	return buf.Bytes(), nil
}

func ListPostUpgradeTasks() ([]string, error) {
	fileMatch, err := filepath.Glob(filepath.Join("ci", "post-update.sh"))
	if err != nil {
		return nil, err
	}

	dirMatches, err := filepath.Glob(filepath.Join("ci", "post-update.d", "*.sh"))
	if err != nil {
		return nil, err
	}

	matches := append(fileMatch, dirMatches...)

	return matches, nil
}
