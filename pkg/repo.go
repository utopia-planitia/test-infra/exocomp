package exocomp

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	cli "github.com/urfave/cli/v2"
	gitlab "github.com/xanzy/go-gitlab"
)

type Repo struct {
	dataPath string
}

func (r Repo) ApplyIssues(issues []Issue, project *gitlab.Project, gitlab Gitlab, c *cli.Context) error {
	cfg, err := r.Config()
	if err != nil {
		return fmt.Errorf("load config: %v", err)
	}

	for _, issue := range issues {
		log.Printf("%s %s\n", string(issue.Emoji()), issue.Name())

		enabled := cfg.Enabled(issue)
		if !enabled {
			log.Printf("issue is disabled")
			continue
		}

		err := r.Cleanup(project.DefaultBranch)
		if err != nil {
			return fmt.Errorf("cleanup working directory: %v", err)
		}

		issueState, err := issue.Apply(r, project, c)
		if err != nil {
			return fmt.Errorf("verify possible issue: %v", err)
		}

		existingIssue, err := gitlab.LookupIssue(project, issueState.Title)
		if err != nil {
			return fmt.Errorf("look up issue: %v", err)
		}

		// issue was opened before
		if existingIssue != nil {
			_, err = gitlab.UpdateIssue(project, existingIssue.IID, issueState, issue.Name())
			if err != nil {
				return fmt.Errorf("update issue: %v", err)
			}

			continue
		}

		// issue was not opened before and should not be opened
		if !issueState.Open {
			continue
		}

		// issue was not opened before and should be opend
		_, err = gitlab.CreateIssue(project, issueState, issue.Name())
		if err != nil {
			return fmt.Errorf("create missing issue: %v", err)
		}
	}

	return nil
}

func (r Repo) ApplyCodeChanges(codeChanges []CodeChange, project *gitlab.Project, gitlab Gitlab, disablePush, disableAutomerge bool, c *cli.Context) error {
	cfg, err := r.Config()
	if err != nil {
		return fmt.Errorf("load config: %v", err)
	}

	for _, cc := range codeChanges {
		branchName := cc.BranchName()

		log.Printf("%s %s\n", string(cc.Emoji()), cc.Name())

		enabled := cfg.Enabled(cc)
		if !enabled {
			log.Printf("codeChange is disabled")
			continue
		}

		err := r.Cleanup(project.DefaultBranch)
		if err != nil {
			return fmt.Errorf("cleanup working directory: %v", err)
		}

		err = r.UseBranch(branchName, project.DefaultBranch)
		if err != nil {
			return fmt.Errorf("switch to branch %s: %v", branchName, err)
		}

		mr, err := gitlab.LookupMergeRequest(project, branchName, project.DefaultBranch)
		if err != nil {
			return fmt.Errorf("create merge request: %v", err)
		}

		commitMsg, err := cc.Apply(r, project, mr, c)
		if err != nil {
			return fmt.Errorf("apply codeChanges changes: %v", err)
		}

		hasChanges, err := r.HasChanges()
		if err != nil {
			return fmt.Errorf("diff for changes: %v", err)
		}

		if hasChanges {
			err = r.Commit(commitMsg)
			if err != nil {
				return fmt.Errorf("commit changes: %v", err)
			}
		}

		branchHasChanges := r.BranchHasChangesTo(branchName, project.DefaultBranch)
		if !branchHasChanges {
			continue
		}

		if disablePush {
			log.Println("push: skipped because the `--disable-push` flag is set to true.")
			continue
		}

		err = r.Push(branchName)
		if err != nil {
			return fmt.Errorf("push branch %s to remote: %v", branchName, err)
		}

		if mr == nil {
			mr, err = gitlab.CreateMergeRequest(project, cc)
			if err != nil {
				return fmt.Errorf("create merge request: %v", err)
			}
		}

		mr, err = gitlab.ReloadMergeRequest(project, mr)
		if err != nil {
			return fmt.Errorf("reload merge request: %v", err)
		}

		if !mr.MergeWhenPipelineSucceeds && cc.AutomergeGitlabCI() {
			mr, err = gitlab.EnableGitlabAutomerge(project, mr, disableAutomerge)
			if err != nil {
				return fmt.Errorf("enable gitlab automerge: %v", err)
			}
		}

		_ = mr
	}

	return nil
}

func (r Repo) ExecuteScans(scans []Scan, project *gitlab.Project, gitlab Gitlab, c *cli.Context) error {
	cfg, err := r.Config()
	if err != nil {
		return fmt.Errorf("load config: %v", err)
	}

	for _, scan := range scans {
		// log.Printf("%s start executing \"%s\" command\n", string(scan.Emoji()), scan.Name())

		enabled := cfg.Enabled(scan)
		if !enabled {
			log.Printf("scan is disabled")
			continue
		}

		err := r.Cleanup(project.DefaultBranch)
		if err != nil {
			return fmt.Errorf("cleanup working directory: %v", err)
		}

		err = scan.Execute(&r, project, nil, &gitlab, c)
		if err != nil {
			return fmt.Errorf("couldn't execute '%s' Scan, error: %v", scan.Name(), err)
		}
	}

	return nil
}

func (r Repo) WorkingDir() string {
	return r.dataPath
}

func (r Repo) Execute(command string, args ...string) error {
	cmd := exec.Command(command, args...)
	cmd.Dir = r.dataPath

	// log.Println(cmd)

	return wrapError(cmd)
}

func (r Repo) Script(script string, args ...interface{}) error {
	cmd := exec.Command("bash", "-x")
	evaluatedScript := fmt.Sprintf(script, args...)
	cmd.Stdin = strings.NewReader(evaluatedScript)
	cmd.Dir = r.dataPath

	return wrapError(cmd)
}

func wrapError(cmd *exec.Cmd) error {
	output, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("command %s failed: %v: %s", cmd.String(), err, output)
	}

	return nil
}

func (r Repo) Fetch() error {
	log.Printf("💬 fetch updates")

	// Git commands have a per-minute rate limit so we only need to wait up to 60 seconds
	// https://docs.gitlab.com/ee/security/rate_limits.html#git-operations-using-ssh
	sleep := 10 * time.Second
	remainingRetries := 6

	for {
		err := r.Execute("git", "fetch", "--all", "--prune")
		if err != nil && remainingRetries > 0 && isRateLimitError(err) {
			log.Printf("💬 retry in %.0f seconds due to rate limit (remaining retries: %d)", sleep.Seconds(), remainingRetries)
			remainingRetries--
			time.Sleep(sleep)
			continue
		}

		return err
	}
}

func isRateLimitError(err error) bool {
	return strings.Contains(fmt.Sprint(err), "returned error: 429")
}

func (r Repo) Cleanup(mainBranch string) error {
	return r.Script(`set -euxo pipefail
git clean -f
git reset --hard origin/%s`, mainBranch)
}

func (r Repo) UseBranch(branchName, mainBranch string) error {
	return r.Script(`set -euxo pipefail
	if git checkout %s; then
		git reset --hard origin/%s || true
		if ! git rebase origin/%s; then
			git rebase --abort
			git reset --hard origin/%s
		fi
	else
		git checkout -b %s origin/%s
	fi`, branchName, branchName, mainBranch, mainBranch, branchName, mainBranch)
}

func (r Repo) HasChanges() (bool, error) {
	cmd := exec.Command("git", "status", "--porcelain")

	output, err := cmd.CombinedOutput()
	if err != nil {
		return false, fmt.Errorf("check git status: %v: %s", err, output)
	}

	return len(output) != 0, nil
}

func (r Repo) IsEmpty() (bool, error) {
	cmd := exec.Command("bash")
	cmd.Stdin = strings.NewReader("find .git/objects -type f | wc -l")

	output, err := cmd.CombinedOutput()
	if err != nil {
		return false, fmt.Errorf("check if repo is empty: %v: %s", err, output)
	}

	return string(output) == "0\n", nil
}

func (r Repo) Commit(msg string) error {
	err := r.Execute("git", "add", ".")
	if err != nil {
		return err
	}

	return r.Execute("git", "commit", "-m", msg)
}

func (r Repo) ConfigureRemote(url string) error {
	log.Printf("💬 configure remote")
	return r.Execute("git", "remote", "set-url", "origin", url)
}

func (r Repo) BranchHasChangesTo(branch, mainBranch string) bool {
	err := r.Script("git diff %s origin/%s --exit-code > /dev/null", branch, mainBranch)
	return err != nil
}

func (r Repo) Push(branch string) error {
	return r.Script("git diff %s origin/%s --exit-code > /dev/null || git push origin %s -f", branch, branch, branch)
}

func (r Repo) HasHelmfiles() (bool, error) {
	helmfiles, err := r.ListHelmfiles()
	if err != nil {
		return false, err
	}

	hasHelmfiles := len(helmfiles) > 0

	return hasHelmfiles, nil
}

func (r Repo) ListHelmfiles() ([]string, error) {
	allHelmfiles := []string{}

	yamlMatches, err := filepath.Glob(filepath.Join("*", "helmfile.yaml"))
	if err != nil {
		return nil, err
	}

	allHelmfiles = append(allHelmfiles, yamlMatches...)

	tplMatches, err := filepath.Glob(filepath.Join("*", "helmfile.yaml.tpl"))
	if err != nil {
		return nil, err
	}

	allHelmfiles = append(allHelmfiles, tplMatches...)

	return allHelmfiles, nil
}

func FileExists(name string) (bool, error) {
	_, err := os.Stat(name)

	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}

	return false, err
}
