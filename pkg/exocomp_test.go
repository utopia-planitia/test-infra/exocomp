package exocomp

import (
	"testing"

	"github.com/urfave/cli/v2"
	gitlab "github.com/xanzy/go-gitlab"
)

type testTask struct{}

func (f testTask) New(_ *cli.Context) Task { return f }
func (f testTask) Enabled() bool           { return false }
func (f testTask) Batchable() bool         { return false }
func (f testTask) Name() string            { return "fake" }
func (f testTask) Emoji() rune             { return 'ö' }
func (f testTask) Scope() TaskScope        { return TaskScopeProject }
func (f testTask) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

type testCodeChange struct {
	testTask
}

func (t testCodeChange) MergeRequestTitle() string { return "" }
func (t testCodeChange) MergeRequestBody() string  { return "" }
func (t testCodeChange) BranchName() string        { return "" }
func (t testCodeChange) AutomergeGitlabCI() bool   { return false }
func (t testCodeChange) Apply(r Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	return "", nil
}

type testIssue struct {
	testTask
}

func (t testIssue) Apply(r Repo, p *gitlab.Project, c *cli.Context) (IssueState, error) {
	return IssueState{}, nil
}

type testScan struct {
	testTask
}

func (scan testScan) Flags() []cli.Flag { return []cli.Flag{} }
func (scan testScan) Execute(r *Repo, p *gitlab.Project, group *gitlab.Group, g *Gitlab, c *cli.Context) error {
	return nil
}

func TestTasks_hasOnlyCodeChangesIssuesAndScans(t *testing.T) {
	// type T struct{}
	// var _ I = T{}       // Verify that T implements I.
	// var _ I = (*T)(nil) // Verify that *T implements I.

	// Verify that interfaces are imüplemented
	var _ Task = testTask{}
	var _ Task = (*testTask)(nil)

	var _ Issue = testIssue{}
	var _ Issue = (*testIssue)(nil)

	var _ CodeChange = testCodeChange{}
	var _ CodeChange = (*testCodeChange)(nil)

	var _ Scan = testScan{}
	var _ Scan = (*testScan)(nil)

	tests := []struct {
		name  string
		tasks Tasks
		want  bool
	}{
		{
			name: "has Task and should return false",
			tasks: Tasks{
				testTask{},
			},
			want: false,
		},
		{
			name: "has CodeChange and should return true",
			tasks: Tasks{
				testCodeChange{},
			},
			want: true,
		},
		{
			name: "has Issue and should return true",
			tasks: Tasks{
				testIssue{},
			},
			want: true,
		},
		{
			name: "has Scans and should return true",
			tasks: Tasks{
				testScan{},
			},
			want: true,
		},
		{
			name: "has only Tasks and should return false",
			tasks: Tasks{
				testTask{},
				testTask{},
			},
			want: false,
		},
		{
			name: "has only CodeChanges and should return true",
			tasks: Tasks{
				testCodeChange{},
				testCodeChange{},
			},
			want: true,
		},
		{
			name: "has only Issues and should return true",
			tasks: Tasks{
				testIssue{},
				testIssue{},
			},
			want: true,
		},
		{
			name: "has CodeChanges and Tasks and should return false",
			tasks: Tasks{
				testTask{},
				testCodeChange{},
			},
			want: false,
		},
		{
			name: "has Issues and Tasks and should return false",
			tasks: Tasks{
				testTask{},
				testIssue{},
			},
			want: false,
		},
		{
			name: "has Issues, CodeChanges and Tasks and should return false",
			tasks: Tasks{
				testTask{},
				testCodeChange{},
				testIssue{},
			},
			want: false,
		},
		{
			name: "has Issues and CodeChanges only and should return true",
			tasks: Tasks{
				testCodeChange{},
				testIssue{},
			},
			want: true,
		},
		{
			name: "has CodeChanges and Scans and should return true",
			tasks: Tasks{
				testScan{},
				testCodeChange{},
			},
			want: true,
		},
		{
			name: "has CodeChanges, Issues and Scans and should return true",
			tasks: Tasks{
				testScan{},
				testCodeChange{},
				testIssue{},
			},
			want: true,
		},
		{
			name: "has Scans only and should return true",
			tasks: Tasks{
				testScan{},
				testScan{},
			},
			want: true,
		},
		{
			name:  "has nothing and should return true",
			tasks: Tasks{},
			want:  true,
		},
		{
			name: "has Scans and Tasks and should return false",
			tasks: Tasks{
				testTask{},
				testScan{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tasks.hasOnlyCodeChangesIssuesAndScans(); got != tt.want {
				t.Errorf("Tasks.hasOnlyCodeChangesIssuesAndScans() = %v, want %v", got, tt.want)
			}
		})
	}
}
