package exocomp

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
)

type Repos struct {
	dataPath string
}

func NewRepos(dataPath string) (Repos, error) {
	dataPath, err := filepath.Abs(dataPath)
	if err != nil {
		return Repos{}, err
	}

	err = os.MkdirAll(dataPath, os.ModePerm)
	if err != nil {
		return Repos{}, err
	}

	return Repos{
		dataPath: dataPath,
	}, nil
}

func (rr Repos) DownloadRepo(token string, gitlabProject *gitlab.Project) (Repo, error) {

	gitlabProjectUrl, err := url.Parse(gitlabProject.WebURL)
	if err != nil {
		return Repo{}, fmt.Errorf("couldn't parse gitlab project WebURL: %v", err)
	}

	gitUrl, err := url.Parse(gitlabProject.HTTPURLToRepo)
	if err != nil {
		return Repo{}, err
	}

	gitUrl.User = url.UserPassword("gitlab-ci-token", token)

	dataDirectory := filepath.Join(rr.dataPath, gitlabProjectUrl.Hostname(), gitlabProject.PathWithNamespace)

	_, err = os.Stat(dataDirectory)
	if os.IsNotExist(err) {
		log.Printf("🐑 cloning git repo")

		err = os.MkdirAll(filepath.Dir(dataDirectory), os.ModePerm)
		if err != nil {
			return Repo{}, err
		}

		// Git commands have a per-minute rate limit so we only need to wait up to 60 seconds
		// https://docs.gitlab.com/ee/security/rate_limits.html#git-operations-using-ssh
		// TODO? The same retry logic is already implemented in repo.go, too. Can and should we extract and reuse it?
		sleep := 10 * time.Second
		remainingRetries := 6

		for {
			cmd := exec.Command("git", "clone", gitUrl.String(), dataDirectory)
			cmd.Dir = filepath.Dir(dataDirectory)
			err = wrapError(cmd)
			if err != nil && remainingRetries > 0 && isRateLimitError(err) {
				log.Printf("💬 retry in %.0f seconds due to rate limit (remaining retries: %d)", sleep.Seconds(), remainingRetries)
				remainingRetries--
				time.Sleep(sleep)
				continue
			}
			if err != nil {
				return Repo{}, fmt.Errorf("git clone: %v", err)
			}

			return Repo{dataPath: dataDirectory}, nil
		}
	}

	if err != nil {
		return Repo{}, err
	}

	repo := Repo{dataPath: dataDirectory}

	err = repo.ConfigureRemote(gitUrl.String())
	if err != nil {
		return Repo{}, err
	}

	err = repo.Fetch()
	if err != nil {
		return Repo{}, err
	}

	return repo, nil
}
