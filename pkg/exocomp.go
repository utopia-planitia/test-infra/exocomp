package exocomp

import (
	"errors"
	"fmt"
	"log"
	"os"

	cli "github.com/urfave/cli/v2"
	gitlab "github.com/xanzy/go-gitlab"
)

type IssueState struct {
	Open  bool
	Title string
	Body  string
}

// TaskScope is just a new dimension which implies on which level we now operate the tasks on gitlab.
// Instead of only on project level scope we now want to be able to also run tasks on other gitlab conepts, e.g. gitlab groups
// or maybe in the furture account management
type TaskScope uint8

const (
	TaskScopeGroup TaskScope = iota
	TaskScopeProject
)

type Task interface {
	New(c *cli.Context) Task
	Enabled() bool
	// Batchable should return true to indicate that the task can be run in batch with other tasks (intention for run "all" command), otherwise false
	Batchable() bool
	Name() string
	Emoji() rune
	// Scope returns the gitlab API resource scope e.g. groups or projects, in which the task operates
	Scope() TaskScope
	// Flags returns a list of cli.Flag which can be passed to exocomp's subcommands
	Flags() []cli.Flag
}

type Tasks []Task

type Issue interface {
	Task //embeded interface

	// Apply applies changes for issues in gitlab
	Apply(r Repo, p *gitlab.Project, c *cli.Context) (IssueState, error)
}

type CodeChange interface {
	Task //embeded interface

	MergeRequestTitle() string
	MergeRequestBody() string
	BranchName() string
	AutomergeGitlabCI() bool

	// Apply applies changes for repositories gitlab
	Apply(r Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (commitMsg string, err error)
}

type Scan interface {
	Task //embeded interface

	// Execute is intended to execute against local repo, e.g. find, search,...
	Execute(r *Repo, p *gitlab.Project, group *gitlab.Group, g *Gitlab, c *cli.Context) error
}

type GitlabProjectsOptions struct {
	OnlyMembershipRepos  bool
	ExcludePersonalRepos bool
	LimitToProject       int
}

// Exocomp makes code-changes, creates gitlab issues and scans repositories.
type Exocomp struct {
	GitlabToken string
	DataDir     string
}

func NewExocomp(token, dataDir string) (Exocomp, error) {
	if token == "" {
		return Exocomp{}, fmt.Errorf("GITLAB_TOKEN is empty")
	}
	if dataDir == "" {
		return Exocomp{}, fmt.Errorf("DATA_PATH is empty")
	}

	return Exocomp{
		GitlabToken: token,
		DataDir:     dataDir,
	}, nil
}

// Execute makes code-changes, creates gitlab issues and scans repositories
func (e Exocomp) Execute(tasks Tasks, gitlabBaseUrl string, gitlabProjectsOptions GitlabProjectsOptions, disablePush, disableAutomerge bool, c *cli.Context) error {
	if len(tasks) == 0 {
		log.Println("skip execute: the list of code-changes, issues etc. is empty so there is nothing to do")
		return nil
	}

	if !tasks.hasOnlyCodeChangesIssuesAndScans() {
		return errors.New("we got something else than code-changes, issues or scans in Tasks")
	}

	log.Printf("⚙️ searching for groups and projects at '%s'", gitlabBaseUrl)

	repos, err := NewRepos(e.DataDir)
	if err != nil {
		return fmt.Errorf("initialize repos struct: %v", err)
	}

	var baseUrlOption gitlab.ClientOptionFunc
	baseUrlOption = func(c *gitlab.Client) error {
		return nil
	}

	if gitlabBaseUrl != "" {
		baseUrlOption = gitlab.WithBaseURL(gitlabBaseUrl)
	}

	gitlabCLient, err := gitlab.NewClient(e.GitlabToken, baseUrlOption)
	if err != nil {
		return fmt.Errorf("setup gitlab client: %v", err)
	}

	gitlab := Gitlab{
		client: gitlabCLient,
	}

	projects, err := gitlab.Projects(gitlabProjectsOptions)
	if err != nil {
		return fmt.Errorf("list gitlab projects: %v", err)
	}

	groups, err := gitlab.Groups()
	if err != nil {
		return fmt.Errorf("list gitlab groups: %v", err)
	}

	err = executeGroupTasks(tasks.getGroupTasks(), groups, gitlab, c)
	if err != nil {
		return fmt.Errorf("exec group tasks: %v", err)
	}

	err = executeProjectTasks(tasks.getProjectTasks(), repos, projects, e.GitlabToken, gitlab, disablePush, disableAutomerge, c)
	if err != nil {
		return fmt.Errorf("exec group tasks: %v", err)
	}

	return nil
}

func executeGroupTasks(tasks Tasks, groups []*gitlab.Group, gitlab Gitlab, c *cli.Context) error {
	if len(tasks) == 0 {
		return nil
	}

	for _, group := range groups {
		fmt.Println("")
		log.Printf("🎁 Group: %s (#%d) %s", group.Name, group.ID, group.FullPath)

		for _, scan := range tasks.getScans() {
			// log.Printf("%s start executing \"%s\" command\n", string(scan.Emoji()), scan.Name())

			err := scan.Execute(nil, nil, group, &gitlab, c)
			if err != nil {
				return fmt.Errorf("couldn't execute '%s' Scan, error: %v", scan.Name(), err)
			}
		}
	}

	return nil
}

func executeProjectTasks(tasks Tasks, repos Repos, projects []*gitlab.Project, gitlabToken string, gitlab Gitlab, disablePush, disableAutomerge bool, c *cli.Context) error {
	if len(tasks) == 0 {
		return nil
	}

	pluralS := "s"
	if len(projects) == 1 {
		pluralS = ""
	}
	log.Printf("found %d project%s that matched the search criteria", len(projects), pluralS)

	for _, project := range projects {
		fmt.Println("")
		log.Printf("🎁 Project: %s (#%d) %s", project.Name, project.ID, project.PathWithNamespace)

		repo, err := repos.DownloadRepo(gitlabToken, project)
		if err != nil {
			return fmt.Errorf("clone/fetch git repo: %v", err)
		}

		err = os.Chdir(repo.WorkingDir())
		if err != nil {
			return fmt.Errorf("change working directory to \"%s\": %v", repo.WorkingDir(), err)
		}

		isEmpty, err := repo.IsEmpty()
		if err != nil {
			return fmt.Errorf("check if repo is empty: %v", err)
		}

		if isEmpty {
			log.Printf("skipping this project, because the repo is empty")
			continue
		}

		err = repo.ApplyCodeChanges(tasks.getCodeChanges(), project, gitlab, disablePush, disableAutomerge, c)
		if err != nil {
			return err
		}

		err = repo.ApplyIssues(tasks.getIssues(), project, gitlab, c)
		if err != nil {
			return err
		}

		err = repo.ExecuteScans(tasks.getScans(), project, gitlab, c)
		if err != nil {
			return err
		}
	}

	return nil
}

func (tasks Tasks) getProjectTasks() Tasks {
	var projectTasks Tasks
	for _, task := range tasks {
		if task.Scope() == TaskScopeProject {
			projectTasks = append(projectTasks, task)
		}
	}
	return projectTasks
}

func (tasks Tasks) getGroupTasks() Tasks {
	var groupTasks Tasks
	for _, task := range tasks {
		if task.Scope() == TaskScopeGroup {
			groupTasks = append(groupTasks, task)
		}
	}
	return groupTasks
}

func (tasks Tasks) getCodeChanges() []CodeChange {
	var codeChanges []CodeChange
	for _, task := range tasks {
		if cc, ok := task.(CodeChange); ok {
			codeChanges = append(codeChanges, cc)
		}
	}
	return codeChanges
}

func (tasks Tasks) getIssues() []Issue {
	var issues []Issue
	for _, task := range tasks {
		if i, ok := task.(Issue); ok {
			issues = append(issues, i)
		}
	}
	return issues
}

func (tasks Tasks) getScans() []Scan {
	var scans []Scan
	for _, task := range tasks {
		if s, ok := task.(Scan); ok {
			scans = append(scans, s)
		}
	}
	return scans
}

func (tasks Tasks) hasOnlyCodeChangesIssuesAndScans() bool {
	for _, task := range tasks {
		switch task.(type) {
		case CodeChange:
			continue
		case Issue:
			continue
		case Scan:
			continue
		default:
			return false
		}
	}

	return true
}
