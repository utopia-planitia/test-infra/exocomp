package exocomp

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Modules []ModuleConfig
}

type ModuleConfig struct {
	Name    string
	Enabled *bool
}

type Module interface {
	Enabled() bool
	Name() string
}

func (r Repo) Config() (Config, error) {
	exocompYaml, err := os.ReadFile(".exocomp.yaml")
	if err != nil && os.IsNotExist(err) {
		return Config{}, nil
	}
	if err != nil {
		return Config{}, err
	}

	cfg := Config{}

	err = yaml.Unmarshal(exocompYaml, &cfg)
	if err != nil {
		return Config{}, err
	}

	return cfg, nil
}

func (c Config) Enabled(m Module) bool {
	for _, module := range c.Modules {
		if module.Name == m.Name() {
			if module.Enabled == nil {
				return m.Enabled()
			}

			return *module.Enabled
		}
	}

	return m.Enabled()
}
