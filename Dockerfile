FROM ghcr.io/utopia-planitia/renovate-bot-extended-image:latest@sha256:819f36159b42bc5b48a05d5310c98cd4adc7a195cd26cca3036143f6d204b9ee

USER root

# RUN rm /usr/local/bin/chart-prettier
# RUN go get github.com/utopia-planitia/chart-prettier@main
# RUN mv /go/bin/chart-prettier /usr/local/bin/chart-prettier

RUN npm install --save-dev --save-exact --global prettier

USER ubuntu

RUN git config --global user.email "utopiaplanitabot@gmail.com"
RUN git config --global user.name "Exocomp Bot"
