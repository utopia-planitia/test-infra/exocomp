FROM ghcr.io/utopia-planitia/renovate-bot-extended-image:latest@sha256:9329f3782d33dfab139a1cf6c424cd759a26e38cdd5747f5ab9bd7d25e7074f7

USER root

# RUN rm /usr/local/bin/chart-prettier
# RUN go get github.com/utopia-planitia/chart-prettier@main
# RUN mv /go/bin/chart-prettier /usr/local/bin/chart-prettier

RUN npm install --save-dev --save-exact --global prettier

USER ubuntu

RUN git config --global user.email "utopiaplanitabot@gmail.com"
RUN git config --global user.name "Exocomp Bot"
