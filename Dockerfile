FROM ghcr.io/utopia-planitia/renovate-bot-extended-image:latest@sha256:837d3fbd8695c745a1d18ee1fa8d1ce7d8b0bdaa3e6b4b7571b05db22726f3ed

USER root

# RUN rm /usr/local/bin/chart-prettier
# RUN go get github.com/utopia-planitia/chart-prettier@main
# RUN mv /go/bin/chart-prettier /usr/local/bin/chart-prettier

RUN npm install --save-dev --save-exact --global prettier

USER ubuntu

RUN git config --global user.email "utopiaplanitabot@gmail.com"
RUN git config --global user.name "Exocomp Bot"
