package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	cli "github.com/urfave/cli/v2"

	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/chartprettier"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/convert"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/generate"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/gitlabci"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/golang"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/license"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/newline"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/prettier"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/renovate"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/replace"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/cipipeline"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/helmtests"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/mainbranch"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/masterbranch"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/mergecommits"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/readme"
	"gitlab.com/utopia-planitia/test-infra/exocomp/issues/servicetests"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
	"gitlab.com/utopia-planitia/test-infra/exocomp/scans/find"
	"gitlab.com/utopia-planitia/test-infra/exocomp/scans/variables"
)

var (
	gitlabBaseUrlFlag = &cli.StringFlag{
		Name:    "gitlab-base-url",
		Value:   "",
		EnvVars: []string{"GITLAB_BASE_URL"},
	}

	onlyMembershipReposFlag = &cli.BoolFlag{
		Name:    "only-membership-repos",
		Value:   true,
		EnvVars: []string{"EXOCOMP_ONLY_MEMBERSHIP_REPOS"},
	}

	excludePersonalReposFlag = &cli.BoolFlag{
		Name:    "exclude-personal-repos",
		Value:   false,
		EnvVars: []string{"EXOCOMP_EXCLUDE_PERSONAL_REPOS"},
	}

	disableAutomergeFlag = &cli.BoolFlag{
		Name:    "disable-automerge",
		EnvVars: []string{"EXOCOMP_DISABLE_AUTOMERGE"},
	}

	disablePushFlag = &cli.BoolFlag{
		Name:    "disable-push",
		EnvVars: []string{"EXOCOMP_DISABLE_PUSH"},
	}

	limitProjectById = &cli.IntFlag{
		Name:    "limit-to-project",
		Usage:   "specify project ID to perform tasks on, 0 = all projects",
		EnvVars: []string{"EXOCOMP_PROJECT_ID"},
	}
)

func main() {
	err := run()
	if err != nil {
		log.Fatalf("running exocomp: %v", err)
	}
}

func run() error {
	err := loadEnvIfExists(".env")
	if err != nil {
		return fmt.Errorf("load configuration from .env file: %v", err)
	}

	dataDir, dataDirEnvVarIsSet := os.LookupEnv("DATA_PATH")
	if !dataDirEnvVarIsSet {
		log.Println("Using _data as the default data directory. You can change the directory by setting the DATA_PATH environment variable.")
		dataDir = "_data"
	}

	bot, err := exocomp.NewExocomp(
		os.Getenv("GITLAB_TOKEN"),
		dataDir,
	)
	if err != nil {
		return fmt.Errorf("initialize exocomp struct: %v", err)
	}

	codeChanges := allCodeChanges()
	issues := allIssues()
	scans := allScans()

	app := &cli.App{
		Name:  "exocomp",
		Usage: "manage code in a multi repo environment",
		Flags: []cli.Flag{
			gitlabBaseUrlFlag,
			onlyMembershipReposFlag,
			excludePersonalReposFlag,
			limitProjectById,
		},
		Commands: []*cli.Command{
			{
				Name:    "code",
				Aliases: []string{"codechanges", "codechange", "codeChanges", "codeChange"},
				Action: func(c *cli.Context) error {
					return cli.ShowAppHelp(c)
				},
				Flags: []cli.Flag{
					disableAutomergeFlag,
					disablePushFlag,
				},
				Subcommands: commandsFromTasks(bot, codeChanges),
			},
			{
				Name: "issues",
				Action: func(c *cli.Context) error {
					return cli.ShowAppHelp(c)
				},
				Subcommands: commandsFromTasks(bot, issues),
			},
			{
				Name:    "scans",
				Aliases: []string{"scan"},
				Action: func(c *cli.Context) error {
					return cli.ShowAppHelp(c)
				},
				Subcommands: commandsFromTasks(bot, scans),
			},
		},
	}

	return app.Run(os.Args)
}

func commandsFromTasks(bot exocomp.Exocomp, tasks []exocomp.Task) []*cli.Command {

	all_command := cli.Command{
		Name:  "all",
		Usage: "executes all commands",
		Action: func(c *cli.Context) error {
			gitlabBaseURL := c.String(gitlabBaseUrlFlag.Name)
			gitlabProjectsOptions := exocomp.GitlabProjectsOptions{
				OnlyMembershipRepos:  c.Bool(onlyMembershipReposFlag.Name),
				ExcludePersonalRepos: c.Bool(excludePersonalReposFlag.Name),
				LimitToProject:       c.Int(limitProjectById.Name),
			}
			disablePush := c.Bool(disablePushFlag.Name)
			disableAutomerge := c.Bool(disableAutomergeFlag.Name)

			active := []exocomp.Task{}
			for _, task := range tasks {
				//skip task if not enabled (in `all` cmd)
				if !task.Enabled() || !task.Batchable() {
					continue
				}

				active = append(active, task)
			}

			err := bot.Execute(
				active,
				gitlabBaseURL,
				gitlabProjectsOptions,
				disablePush,
				disableAutomerge,
				c,
			)
			if err != nil {
				return fmt.Errorf("executing tasks: %v", err)
			}

			return nil
		},
	}

	commands := []*cli.Command{&all_command}

	for _, task := range tasks {
		command := cli.Command{
			Name: task.Name(),
			Action: func(task exocomp.Task) cli.ActionFunc {
				return func(c *cli.Context) error {
					gitlabBaseURL := c.String(gitlabBaseUrlFlag.Name)
					gitlabProjectsOptions := exocomp.GitlabProjectsOptions{
						OnlyMembershipRepos:  c.Bool(onlyMembershipReposFlag.Name),
						ExcludePersonalRepos: c.Bool(excludePersonalReposFlag.Name),
						LimitToProject:       c.Int(limitProjectById.Name),
					}
					disablePush := c.Bool(disablePushFlag.Name)
					disableAutomerge := c.Bool(disableAutomergeFlag.Name)

					return bot.Execute(
						[]exocomp.Task{task.New(c)},
						gitlabBaseURL,
						gitlabProjectsOptions,
						disablePush,
						disableAutomerge,
						c,
					)
				}
			}(task),
			Flags: task.Flags(),
		}

		commands = append(commands, &command)
	}

	return commands
}

func allCodeChanges() []exocomp.Task {
	return []exocomp.Task{
		license.CodeChange{},
		newline.CodeChange{},
		golang.CodeChange{},
		gitlabci.CodeChange{},
		renovate.CodeChange{},
		convert.IngressCodeChange{},
		convert.RBACCodeChange{},
		generate.CodeChange{},
		prettier.CodeChange{},
		chartprettier.CodeChange{},
		replace.CodeChange{},
	}
}

func allIssues() []exocomp.Task {
	return []exocomp.Task{
		readme.Issue{},
		mainbranch.Issue{},
		masterbranch.Issue{},
		mergecommits.Issue{},
		cipipeline.Issue{},
		helmtests.Issue{},
		servicetests.Issue{},
	}
}

func allScans() []exocomp.Task {
	scans := []exocomp.Scan{
		&find.Scan{},
		&variables.GroupScan{},
		&variables.ProjectScan{},
	}

	tasks := []exocomp.Task{}
	for _, t := range scans {
		tasks = append(tasks, t)
	}

	return tasks
}

func loadEnvIfExists(file string) error {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		return nil
	}

	return godotenv.Load(file)
}
