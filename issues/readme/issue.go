package readme

import (
	_ "embed"
	"os"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.Issue = (*Issue)(nil) // Verify that Issue implements exocomp.Issue.

type Issue struct{}

func (t Issue) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t Issue) Enabled() bool {
	return true
}

func (t Issue) Batchable() bool {
	return true
}

func (t Issue) Name() string {
	return "readme"
}

func (t Issue) Emoji() rune {
	return '📖'
}

func (t Issue) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t Issue) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t Issue) Apply(r exocomp.Repo, p *gitlab.Project, c *cli.Context) (exocomp.IssueState, error) {
	issue := exocomp.IssueState{
		Open:  false,
		Title: "README.md is missing",
		Body: `A readme file helps users to identify the purpose of the project.\n
Please have a look at https://www.makeareadme.com/ for inspiration and add a description.`,
	}

	_, err := os.Stat("README.md")
	if err == nil {
		return issue, nil
	}

	if os.IsNotExist(err) {
		issue.Open = true
		return issue, nil
	}

	return issue, err
}
