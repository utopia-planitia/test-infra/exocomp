package servicetests

import (
	_ "embed"
	"fmt"
	"os"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.Issue = (*Issue)(nil) // Verify that Issue implements exocomp.Issue.

type Issue struct{}

func (t Issue) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t Issue) Enabled() bool {
	return true
}

func (t Issue) Batchable() bool {
	return true
}

func (t Issue) Name() string {
	return "service-tests"
}

func (t Issue) Emoji() rune {
	return '❓'
}

func (t Issue) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t Issue) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t Issue) Apply(r exocomp.Repo, p *gitlab.Project, c *cli.Context) (exocomp.IssueState, error) {
	issue := exocomp.IssueState{
		Open:  false,
		Title: fmt.Sprintf("Service %s is missing tests", p.Name),
		Body:  `Please add a few tests to be able to automatically verify a setup.`,
	}

	found, err := r.HasHelmfiles()
	if err != nil {
		return issue, err
	}

	if !found {
		return issue, nil
	}

	_, err = os.Stat("tests")
	if err == nil {
		return issue, nil
	}

	if os.IsNotExist(err) {
		issue.Open = true
		return issue, nil
	}

	return issue, err
}
