package mergecommits

import (
	_ "embed"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.Issue = (*Issue)(nil) // Verify that Issue implements exocomp.Issue.

type Issue struct{}

func (t Issue) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t Issue) Enabled() bool {
	return true
}

func (t Issue) Batchable() bool {
	return true
}

func (t Issue) Name() string {
	return "use-merge-commits"
}

func (t Issue) Emoji() rune {
	return '🎈'
}

func (t Issue) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t Issue) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t Issue) Apply(r exocomp.Repo, p *gitlab.Project, c *cli.Context) (exocomp.IssueState, error) {
	issue := exocomp.IssueState{
		Open:  false,
		Title: "Use no fast forward merge commits",
		Body:  `Using ff merges or rebased merges slows down updates via renovate bot`,
	}

	issue.Open = p.MergeMethod != gitlab.NoFastForwardMerge

	return issue, nil
}
