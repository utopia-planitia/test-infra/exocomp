package masterbranch

import (
	_ "embed"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.Issue = (*Issue)(nil) // Verify that Issue implements exocomp.Issue.

type Issue struct{}

func (t Issue) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t Issue) Enabled() bool {
	return true
}

func (t Issue) Batchable() bool {
	return true
}

func (t Issue) Name() string {
	return "master-branch"
}

func (t Issue) Emoji() rune {
	return '👻'
}

func (t Issue) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t Issue) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t Issue) Apply(r exocomp.Repo, p *gitlab.Project, c *cli.Context) (exocomp.IssueState, error) {
	issue := exocomp.IssueState{
		Open:  false,
		Title: "The branch \"master\" should be removed",
		Body:  `The primary git branch of a project should be main.\nFor historic reasons many projects use the term \"master\".`,
	}

	exists, err := masterBranchExists()
	if err != nil {
		return issue, err
	}

	issue.Open = exists

	return issue, nil
}

func masterBranchExists() (bool, error) {
	r, err := git.PlainOpen(".")
	if err != nil {
		return false, err
	}

	_, err = r.Reference("refs/remotes/origin/master", true)
	if err == nil {
		return true, nil
	}
	if err.Error() == plumbing.ErrReferenceNotFound.Error() {
		return false, nil
	}

	return false, err
}
