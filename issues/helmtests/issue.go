package helmtests

import (
	_ "embed"
	"os"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.Issue = (*Issue)(nil) // Verify that Issue implements exocomp.Issue.

type Issue struct{}

func (t Issue) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t Issue) Enabled() bool {
	return true
}

func (t Issue) Batchable() bool {
	return true
}

func (t Issue) Name() string {
	return "helm-tests"
}

func (t Issue) Emoji() rune {
	return '🚧'
}

func (t Issue) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t Issue) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t Issue) Apply(r exocomp.Repo, p *gitlab.Project, c *cli.Context) (exocomp.IssueState, error) {
	issue := exocomp.IssueState{
		Open:  false,
		Title: "Migrate docker based tests to helm based tests",
		Body:  `Replacing docker by containerd does not allow us to run tests via docker run anymore.`,
	}

	// look like a service
	found, err := r.HasHelmfiles()
	if err != nil {
		return issue, err
	}

	if !found {
		return issue, nil
	}

	// has tests
	_, err = os.Stat("tests")
	if err != nil && os.IsNotExist(err) {
		return issue, nil
	}
	if err != nil {
		return issue, err
	}

	// tests are up to date
	found, err = testsAreHelmBased()
	if err != nil {
		return issue, err
	}

	issue.Open = !found

	return issue, err
}

func testsAreHelmBased() (bool, error) {
	// tests/helmfile.yaml
	_, err := os.Stat("tests/helmfile.yaml")
	if err == nil {
		return true, nil
	}

	if !os.IsNotExist(err) {
		return false, err
	}

	// tests/helmfile.yaml.tpl
	_, err = os.Stat("tests/helmfile.yaml.tpl")
	if err == nil {
		return true, nil
	}

	if !os.IsNotExist(err) {
		return false, err
	}

	return false, nil
}
