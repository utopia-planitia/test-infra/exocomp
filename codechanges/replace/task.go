package replace

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"unicode/utf8"

	cli "github.com/urfave/cli/v2"
	gitlab "github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var findStringFlag = &cli.StringFlag{
	Name:     "find-string",
	Usage:    "string to find in text files",
	EnvVars:  []string{"EXOCOMP_CODE_REPLACE_FIND_STRING"},
	Required: true,
}

var replaceWithFlag = &cli.StringFlag{
	Name:     "replace-with",
	Usage:    "string to replace to found string with",
	EnvVars:  []string{"EXOCOMP_CODE_REPLACE_REPLACE_WITH"},
	Required: true,
}

var mergeRequestTitleFlag = &cli.StringFlag{
	Name:     "merge-request-title",
	Value:    "exocomp/code-change/find-and-replace (generated by an exocomp execution)",
	EnvVars:  []string{"EXOCOMP_CODE_REPLACE_MERGE_REQUEST_TITLE"},
	Required: false,
}

var mergeRequestDescriptionFlag = &cli.StringFlag{
	Name:     "merge-request-description",
	EnvVars:  []string{"EXOCOMP_CODE_REPLACE_MERGE_REQUEST_DESCRIPTION"},
	Required: false,
}

var branchNameFlag = &cli.StringFlag{
	Name:     "branch-name",
	Value:    "exocomp/code-change/find-and-replace",
	EnvVars:  []string{"EXOCOMP_CODE_REPLACE_BRANCH_NAME"},
	Required: false,
}

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct {
	cli *cli.Context
}

func (t CodeChange) New(c *cli.Context) exocomp.Task {
	return &CodeChange{cli: c}
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return false
}

func (t CodeChange) Name() string {
	return "find-and-replace"
}

func (t CodeChange) Emoji() rune {
	return '🔎'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	flags := []cli.Flag{
		findStringFlag,
		replaceWithFlag,
		mergeRequestTitleFlag,
		mergeRequestDescriptionFlag,
		branchNameFlag,
	}
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	defaultTitle := mergeRequestTitleFlag.Value
	if t.cli == nil {
		return defaultTitle
	}

	mrTitleFlagValue := t.cli.String(mergeRequestTitleFlag.Name)
	if mrTitleFlagValue == "" {
		return defaultTitle
	}

	return mrTitleFlagValue
}

func (t CodeChange) MergeRequestBody() string {
	defaultDescription := mergeRequestDescriptionFlag.Value
	if t.cli == nil {
		return defaultDescription
	}

	mrDescriptionFlagValue := t.cli.String(mergeRequestDescriptionFlag.Name)
	if mrDescriptionFlagValue == "" {
		return defaultDescription
	}

	return mrDescriptionFlagValue
}

func (t CodeChange) BranchName() string {
	defaultBranchName := branchNameFlag.Value
	if t.cli == nil {
		return defaultBranchName
	}

	branchNameFlagValue := t.cli.String(branchNameFlag.Name)
	if branchNameFlagValue == "" {
		return defaultBranchName
	}

	return branchNameFlagValue
}

func (t CodeChange) AutomergeGitlabCI() bool {
	// TODO: Maybe this should not be hardcoded.
	return false
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (commitMessage string, err error) {
	if c == nil {
		return "", fmt.Errorf("find-and-replace: CodeChange.Apply(): expected *cli.Context but got nil")
	}

	find := c.String(findStringFlag.Name)
	if find == "" {
		return "", fmt.Errorf("required flag \"find\" not set or empty")
	}

	replace := c.String(replaceWithFlag.Name)

	// Use only the first 28 characters of each string to keep the commit message shorter than 72 characters.
	// TODO: Could need some logic. What if the strings have (very) different lengths? Should we suffix shortened strings with "..." to show that they have been shortened?
	commitMessage = fmt.Sprintf("replace %.28s with %.28s", find, replace)

	walkDirFn := func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if path == ".git" && d.IsDir() {
			return filepath.SkipDir
		}

		info, err := d.Info()
		if err != nil {
			return err
		}

		allowedFileSize := 128 * MegaByte
		return findAndReplace(path, info, find, replace, allowedFileSize)
	}

	err = filepath.WalkDir(".", walkDirFn)
	if err != nil {
		return "", err
	}

	cmd := exec.Command("git", "--no-pager", "diff", "--unified=0")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		return "", err
	}

	return commitMessage, nil
}

const (
	Byte     int64 = 1
	Kilobyte int64 = 1000 * Byte
	MegaByte       = Kilobyte * 1000
)

type TooLargeError struct {
	Err error
}

func (e *TooLargeError) Error() string {
	return e.Err.Error()
}

func findAndReplace(filePath string, fileInfo fs.FileInfo, find string, replace string, allowedFileSize int64) error {
	if fileInfo.Size() == 0 {
		return nil
	}

	if !fileInfo.Mode().IsRegular() {
		return nil
	}

	skipLargeFile := fileInfo.Size() > allowedFileSize
	if skipLargeFile {
		return &TooLargeError{
			Err: fmt.Errorf("file '%s' with size of '%d' bytes exceeds allowed size of '%d' bytes", filePath, fileInfo.Size(), allowedFileSize),
		}
	}

	originalBytes, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}

	originalContents := string(originalBytes)

	if !utf8.ValidString(originalContents) {
		log.Printf("    [SKIPPED] file '%s' contains non urf encoded runes", filePath)
		return nil
	}

	newContents := strings.Replace(originalContents, find, replace, -1)

	if originalContents == newContents {
		return nil
	}

	err = os.WriteFile(filePath, []byte(newContents), fileInfo.Mode())
	if err != nil {
		return err
	}

	return nil
}
