package replace

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	cli "github.com/urfave/cli/v2"
)

func TestCodeChange_MergeRequestTitle(t *testing.T) {
	type fields struct {
		cli *cli.Context
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "returns merge request title from cli flag",
			fields: fields{
				cli: mockCliContextMergeRequestTitle("merge request title"),
			},
			want: "merge request title",
		},
		{
			name: "returns merge request default title when nil cli.Context",
			fields: fields{
				cli: nil,
			},
			want: mergeRequestTitleFlag.Value,
		},
		{
			name: "returns merge request default title when cli.Context flag not exists",
			fields: fields{
				cli: &cli.Context{},
			},
			want: mergeRequestTitleFlag.Value,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := CodeChange{
				cli: tt.fields.cli,
			}
			if got := tr.MergeRequestTitle(); got != tt.want {
				t.Errorf("CodeChange.MergeRequestTitle() = \"%v\", want \"%v\"", got, tt.want)
			}
		})
	}
}

func mockCliContext(flagSet flag.FlagSet) *cli.Context {
	return cli.NewContext(&cli.App{}, &flagSet, &cli.Context{})
}

func mockCliContextMergeRequestTitle(mergeRequestTitleValue string) *cli.Context {
	flagSet := flag.FlagSet{}
	defaultFlagValue := "default"
	flagUsage := ""
	var p string

	flagSet.StringVar(&p, mergeRequestTitleFlag.Name, defaultFlagValue, flagUsage)

	err := flagSet.Set(mergeRequestTitleFlag.Name, mergeRequestTitleValue)
	if err != nil {
		panic(err)
	}

	return mockCliContext(flagSet)
}

func mockCliContextMergeRequestDescription(mergeRequestDescriptionValue string) *cli.Context {
	flagSet := flag.FlagSet{}
	defaultFlagValue := "default"
	flagUsage := ""
	var p string

	flagSet.StringVar(&p, mergeRequestDescriptionFlag.Name, defaultFlagValue, flagUsage)

	err := flagSet.Set(mergeRequestDescriptionFlag.Name, mergeRequestDescriptionValue)
	if err != nil {
		panic(err)
	}

	return mockCliContext(flagSet)
}

func mockCliContextBranchName(branchNameValue string) *cli.Context {
	flagSet := flag.FlagSet{}
	defaultFlagValue := "default"
	flagUsage := ""
	var p string

	flagSet.StringVar(&p, branchNameFlag.Name, defaultFlagValue, flagUsage)

	err := flagSet.Set(branchNameFlag.Name, branchNameValue)
	if err != nil {
		panic(err)
	}

	return mockCliContext(flagSet)
}

func TestCodeChange_MergeRequestBody(t *testing.T) {
	type fields struct {
		cli *cli.Context
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "returns merge request description from cli flag",
			fields: fields{
				cli: mockCliContextMergeRequestDescription("merge request description"),
			},
			want: "merge request description",
		},
		{
			name: "returns merge request default description when nil cli.Context",
			fields: fields{
				cli: nil,
			},
			want: mergeRequestDescriptionFlag.Value,
		},
		{
			name: "returns merge request default description when cli.Context flag not exists",
			fields: fields{
				cli: &cli.Context{},
			},
			want: mergeRequestDescriptionFlag.Value,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := CodeChange{
				cli: tt.fields.cli,
			}
			if got := tr.MergeRequestBody(); got != tt.want {
				t.Errorf("CodeChange.MergeRequestBody() = \"%v\", want \"%v\"", got, tt.want)
			}
		})
	}
}

func TestCodeChange_BranchName(t *testing.T) {
	type fields struct {
		cli *cli.Context
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "returns branch name from cli flag",
			fields: fields{
				cli: mockCliContextBranchName("branch name"),
			},
			want: "branch name",
		},
		{
			name: "returns default branch name when nil cli.Context",
			fields: fields{
				cli: nil,
			},
			want: branchNameFlag.Value,
		},
		{
			name: "returns default branch name when cli.Context flag not exists",
			fields: fields{
				cli: &cli.Context{},
			},
			want: branchNameFlag.Value,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := CodeChange{
				cli: tt.fields.cli,
			}
			if got := tr.BranchName(); got != tt.want {
				t.Errorf("CodeChange.BranchName() = \"%v\", want \"%v\"", got, tt.want)
			}
		})
	}
}

func Test_findAndReplace(t *testing.T) {
	type fileData struct {
		content        string
		createWithSize bool
		size           int64
	}
	type mockData struct {
		file fileData
	}
	type args struct {
		find            string
		replace         string
		allowedFileSize int64
	}
	tests := []struct {
		name             string
		args             args
		mockData         mockData
		wantErr          bool
		wantCheckErrType bool
		wantErrType      error
		wantFileContent  string
	}{
		{
			name: "string is replaced",
			args: args{
				find:            "needel",
				replace:         "needle",
				allowedFileSize: 500 * Kilobyte,
			},
			mockData: mockData{
				file: fileData{
					content:        "this is a haystack with a needel",
					createWithSize: false,
				},
			},
			wantErr:         false,
			wantFileContent: "this is a haystack with a needle",
		},
		{
			name: "string is replaced multiple times",
			args: args{
				find:            "needel",
				replace:         "needle",
				allowedFileSize: 500 * Kilobyte,
			},
			mockData: mockData{
				file: fileData{
					content:        "this is a haystack with a needel and another needel",
					createWithSize: false,
				},
			},
			wantErr:         false,
			wantFileContent: "this is a haystack with a needle and another needle",
		},
		{
			name: "empty file does not cause error",
			args: args{
				find:            "needel",
				replace:         "needle",
				allowedFileSize: 500 * Kilobyte,
			},
			mockData: mockData{
				file: fileData{
					content:        "",
					createWithSize: false,
				},
			},
			wantErr:         false,
			wantFileContent: "",
		},
		{
			name: "too large file causes/returns TooLargeError",
			args: args{
				find:            "needel",
				replace:         "needle",
				allowedFileSize: 500 * Kilobyte,
			},
			mockData: mockData{
				file: fileData{
					createWithSize: true,
					size:           750 * Kilobyte,
				},
			},
			wantErr:          true,
			wantCheckErrType: true,
			wantErrType:      &TooLargeError{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir := t.TempDir()

			fileName := "test.txt"
			filePath := filepath.Join(dir, fileName)

			if tt.mockData.file.createWithSize {
				err := createFileWithSize(filePath, tt.mockData.file.size)
				if err != nil {
					t.Errorf("test setup error: can't create test file '%s' with size '%d': %v", filePath, tt.mockData.file.size, err)
				}
			} else {
				err := os.WriteFile(filePath, []byte(tt.mockData.file.content), 0666)
				if err != nil {
					t.Errorf("test setup error: can't create test file '%s'", filePath)
				}
			}

			fileInfo, err := os.Lstat(filePath)
			if err != nil {
				t.Errorf("test setup error: can't extract file info from test file '%s'", filePath)
			}

			err = findAndReplace(filePath, fileInfo, tt.args.find, tt.args.replace, tt.args.allowedFileSize)

			if ((err != nil) != tt.wantErr) && !tt.wantCheckErrType {
				t.Errorf("findAndReplace() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr && tt.wantCheckErrType {
				errorsHaveSameType := (reflect.TypeOf(tt.wantErrType) == reflect.TypeOf(err))
				if !errorsHaveSameType {
					t.Errorf("findAndReplace() error type = %v, wantErrType %v", reflect.TypeOf(err), reflect.TypeOf(tt.wantErrType))
				}
				return
			}

			fileBytes, err := os.ReadFile(filePath)
			if err != nil {
				t.Errorf("findAndReplace() error: file is unreadable after replace")
			}

			if string(fileBytes) != tt.wantFileContent {
				t.Errorf("findAndReplace() file content = \"%v\", want \"%v\"", string(fileBytes), tt.wantFileContent)
			}
		})
	}
}

func createFileWithSize(filePath string, targetSize int64) error {
	data := make([]byte, int(targetSize))
	f, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("Error: %s", err)
	}
	defer f.Close()
	size, err := f.Write(data) // Write it to the file
	if err != nil {
		return fmt.Errorf("Error: %s", err)
	}

	fmt.Printf("  [info] created file '%s' with size '%d'", filePath, size)

	return nil
}
