package golang

import (
	_ "embed"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct{}

func (t CodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return true
}

func (t CodeChange) Name() string {
	return "format-go"
}

func (t CodeChange) Emoji() rune {
	return '🐨'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	return "formatted go code"
}

func (t CodeChange) MergeRequestBody() string {
	return ""
}

func (t CodeChange) BranchName() string {
	return "exocomp/gofmt"
}

func (t CodeChange) AutomergeGitlabCI() bool {
	return false
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	err := r.Script(`gofmt -s -w .`)
	if err != nil {
		return "", err
	}

	return "used gofmt to formatted go code", nil
}
