package convert

import (
	_ "embed"
	"os"
	"path/filepath"
	"regexp"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.CodeChange = (*RBACCodeChange)(nil) // Verify that RBACCodeChange implements exocomp.CodeChange.

type RBACCodeChange struct{}

func (t RBACCodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t RBACCodeChange) Enabled() bool {
	return true
}

func (t RBACCodeChange) Batchable() bool {
	return true
}

func (t RBACCodeChange) Name() string {
	return "rbac-authorization-v1"
}

func (t RBACCodeChange) Emoji() rune {
	return '🧹'
}

func (t RBACCodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t RBACCodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t RBACCodeChange) MergeRequestTitle() string {
	return "migrate rbac to api version rbac.authorization.k8s.io/v1"
}

func (t RBACCodeChange) MergeRequestBody() string {
	return ""
}

func (t RBACCodeChange) BranchName() string {
	return "exocomp/rbac-authorization-v1"
}

func (t RBACCodeChange) AutomergeGitlabCI() bool {
	return false
}

func (t RBACCodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	fileMatch := regexp.MustCompile(`.*(role|role\-binding).*\.ya?ml`)

	err := filepath.Walk(".",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if !fileMatch.Match([]byte(filepath.Base(path))) {
				return nil
			}

			if !info.Mode().IsRegular() {
				return nil
			}

			oldFound, err := fileContainsString(path, "apiVersion: rbac.authorization.k8s.io/v1beta1")
			if err != nil {
				return err
			}

			if !oldFound {
				return nil
			}

			err = kubectlConvert(path, "rbac.authorization.k8s.io/v1")
			if err != nil {
				return err
			}

			err = yq(path, "del(.metadata.creationTimestamp)")
			if err != nil {
				return err
			}

			return nil
		})
	if err != nil {
		return "", err
	}

	return "migrate rbac to api version rbac.authorization.k8s.io/v1", nil
}
