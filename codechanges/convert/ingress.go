package convert

import (
	"bytes"
	_ "embed"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"al.essio.dev/pkg/shellescape"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.CodeChange = (*IngressCodeChange)(nil) // Verify that IngressCodeChange implements exocomp.CodeChange.

type IngressCodeChange struct{}

func (t IngressCodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t IngressCodeChange) Enabled() bool {
	return true
}

func (t IngressCodeChange) Batchable() bool {
	return true
}

func (t IngressCodeChange) Name() string {
	return "ingress-networking-v1"
}

func (t IngressCodeChange) Emoji() rune {
	return '🧹'
}

func (t IngressCodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t IngressCodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t IngressCodeChange) MergeRequestTitle() string {
	return "migrate ingress to api version networking.k8s.io/v1"
}

func (t IngressCodeChange) MergeRequestBody() string {
	return ""
}

func (t IngressCodeChange) BranchName() string {
	return "exocomp/ingress-networking-v1"
}

func (t IngressCodeChange) AutomergeGitlabCI() bool {
	return false
}

func (t IngressCodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	ingressMatch := regexp.MustCompile(`.*ingress.*\.ya?ml`)

	err := filepath.Walk(".",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if !ingressMatch.Match([]byte(filepath.Base(path))) {
				return nil
			}

			if !info.Mode().IsRegular() {
				return nil
			}

			oldFound, err := fileContainsString(path, "apiVersion: extensions/v1beta1")
			if err != nil {
				return err
			}

			if !oldFound {
				return nil
			}

			err = kubectlConvert(path, "networking.k8s.io/v1")
			if err != nil {
				return err
			}

			err = yq(path, "del(.status)")
			if err != nil {
				return err
			}

			err = yq(path, "del(.metadata.creationTimestamp)")
			if err != nil {
				return err
			}

			return nil
		})
	if err != nil {
		return "", err
	}

	return "migrate ingress to networking/v1 api", nil
}

func kubectlConvert(filename, outputVersion string) error {
	script := fmt.Sprintf(`cat <<< $(kubectl-convert --filename %s --output-version %s) > %s`, shellescape.Quote(filename), shellescape.Quote(outputVersion), shellescape.Quote(filename))

	cmd := exec.Command("bash")
	cmd.Stdin = strings.NewReader(script)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func fileContainsString(file, needle string) (bool, error) {
	b, err := os.ReadFile(file)
	if err != nil {
		return false, err
	}

	return bytes.Contains(b, []byte(needle)), nil
}

func yq(filename, action string) error {
	cmd := exec.Command("yq", "--yml-output", "--yml-roundtrip", "--width=160", "--in-place", action, filename)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
