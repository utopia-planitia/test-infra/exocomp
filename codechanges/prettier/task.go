package prettier

// https://github.com/prettier/prettier

import (
	_ "embed"
	"os"

	"al.essio.dev/pkg/shellescape"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/generate"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct{}

func (t CodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return true
}

func (t CodeChange) Name() string {
	return "prettier"
}

func (t CodeChange) Emoji() rune {
	return '✨'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	return "formatted by running prettier"
}

func (t CodeChange) MergeRequestBody() string {
	return "Used https://github.com/prettier/prettier to improve formatting."
}

func (t CodeChange) BranchName() string {
	return "exocomp/prettier"
}

func (t CodeChange) AutomergeGitlabCI() bool {
	return false
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	err := r.Script(`prettier --write .`)
	if err != nil {
		return "", err
	}

	err = generate.Regenerate(r)
	if err != nil {
		return "", err
	}

	return "used prettier to improve formatting", nil
}

func UpdatePrettierignoreAdd(r exocomp.Repo, line string) error {
	line = shellescape.Quote(line)

	_, err := os.Stat(".prettierignore")
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if err != nil && os.IsNotExist(err) {
		return r.Script(`echo %s > .prettierignore`, line)
	}

	return r.Script(`grep -x %s .prettierignore || ( echo %s >> .prettierignore )`, line, line)
}

func UpdatePrettierignoreRemove(r exocomp.Repo, line string) error {
	line = shellescape.Quote(line)

	_, err := os.Stat(".prettierignore")
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if err != nil && os.IsNotExist(err) {
		return nil
	}

	return r.Script(`grep -v -x %s .prettierignore > .prettierignore1 && mv .prettierignore1 .prettierignore`, line)
}
