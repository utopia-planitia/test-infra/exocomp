package license

import (
	_ "embed"
	"os"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

//go:embed LICENSE.txt
var license string

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct{}

func (t CodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return true
}

func (t CodeChange) Name() string {
	return "license"
}

func (t CodeChange) Emoji() rune {
	return '🤝'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	return "use Apache2 license"
}

func (t CodeChange) MergeRequestBody() string {
	return ""
}

func (t CodeChange) BranchName() string {
	return "exocomp/apache2-license"
}

func (t CodeChange) AutomergeGitlabCI() bool {
	return false
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	err := os.WriteFile("LICENSE", []byte(license), 0o644)
	if err != nil {
		return "", err
	}

	return "use Apache2 license", nil
}
