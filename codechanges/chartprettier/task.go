package chartprettier

import (
	_ "embed"
	"path/filepath"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"github.com/yargevad/filepathx"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct{}

func (t CodeChange) New(c *cli.Context) exocomp.Task {
	return t
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return true
}

func (t CodeChange) Name() string {
	return "chart-prettier"
}

func (t CodeChange) Emoji() rune {
	return '📊'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	return "use chart-prettier to organize chart templates files"
}

func (t CodeChange) MergeRequestBody() string {
	return ""
}

func (t CodeChange) BranchName() string {
	return "exocomp/chart-prettier"
}

func (t CodeChange) AutomergeGitlabCI() bool {
	return false
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	templateDirectories, err := ListChartTemplateDirectories()
	if err != nil {
		return "", err
	}

	for _, templateDirectory := range templateDirectories {
		err := r.Execute(`chart-prettier`, templateDirectory)
		if err != nil {
			return "", err
		}
	}

	return "used chart-prettier to organize chart templates files", nil
}

func ListChartTemplateDirectories() ([]string, error) {
	chartFiles, err := filepathx.Glob("./**/Chart.yaml")
	if err != nil {
		return nil, err
	}

	allTemplateDirs := []string{}

	for _, chartFile := range chartFiles {
		chartDir := filepath.Dir(chartFile)
		templatesDir := filepath.Join(chartDir, "templates")

		found, err := exocomp.FileExists(templatesDir)
		if err != nil {
			return []string{}, err
		}

		if found {
			allTemplateDirs = append(allTemplateDirs, templatesDir)
		}
	}

	return allTemplateDirs, nil
}
