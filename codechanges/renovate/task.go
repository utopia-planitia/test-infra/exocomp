package renovate

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"al.essio.dev/pkg/shellescape"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/chartprettier"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/prettier"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
)

const RENOVATE_JSON = "renovate.json"

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct{}

func (t CodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return true
}

func (t CodeChange) Name() string {
	return "renovate-config"
}

func (t CodeChange) Emoji() rune {
	return '🤖'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	return "configure renovate bot"
}

func (t CodeChange) MergeRequestBody() string {
	return ""
}

func (t CodeChange) BranchName() string {
	return "exocomp/renovate-config"
}

func (t CodeChange) AutomergeGitlabCI() bool {
	return true
}

type RenovateBotTemplateData struct {
	PostUpgradeTasks []string
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	if _, err := os.Stat(RENOVATE_JSON); err != nil {
		return "", nil
	}

	err := t.addSchema()
	if err != nil {
		return "", fmt.Errorf("add renovate json schema: %v", err)
	}

	err = t.addConfigBestPractices()
	if err != nil {
		return "", fmt.Errorf("add config best practices: %v", err)
	}

	err = t.pinContainerImageDigest()
	if err != nil {
		return "", fmt.Errorf("pin container images: %v", err)
	}

	err = t.enableGoModTidy()
	if err != nil {
		return "", fmt.Errorf("enable go mod tidy: %v", err)
	}

	err = t.enableGomodUpdateImportPaths()
	if err != nil {
		return "", fmt.Errorf("enable go mod update import paths: %v", err)
	}

	err = t.enableSubmodules()
	if err != nil {
		return "", fmt.Errorf("enable submodule updates: %v", err)
	}

	err = t.disableHourlyPRLimit()
	if err != nil {
		return "", fmt.Errorf("disable hourly PR limit: %v", err)
	}

	err = t.setConcurrentLimit()
	if err != nil {
		return "", fmt.Errorf("set concurrent PR limit: %v", err)
	}

	err = t.deleteGitlabCIAutomerge()
	if err != nil {
		return "", fmt.Errorf("delete gitlab ci automerge: %v", err)
	}

	err = t.setPlatformAutomerge()
	if err != nil {
		return "", fmt.Errorf("set platform automerge: %v", err)
	}

	err = t.addRenovateLabel()
	if err != nil {
		return "", fmt.Errorf("add labels: %v", err)
	}

	err = t.enableAutomerge()
	if err != nil {
		return "", fmt.Errorf("enable automerge: %v", err)
	}

	err = t.disableDependencyDashboard()
	if err != nil {
		return "", fmt.Errorf("disable dependency dashboard: %v", err)
	}

	err = t.kubernetesUpdates()
	if err != nil {
		return "", fmt.Errorf("enable Kubernetes updates: %v", err)
	}

	err = t.configurePostUpgradeTasks()
	if err != nil {
		return "", fmt.Errorf("configure post-upgrade tasks: %v", err)
	}

	err = prettier.UpdatePrettierignoreAdd(r, RENOVATE_JSON)
	if err != nil {
		return "", fmt.Errorf("update prettier ignore: %v", err)
	}

	return "configured renovate bot", nil
}

func (t CodeChange) addSchema() error {
	return jq(RENOVATE_JSON, `. += {"$schema": "https://docs.renovatebot.com/renovate-schema.json"}`)
}

func (t CodeChange) addConfigBestPractices() error {
	return jq(RENOVATE_JSON, `.extends |= (. - ["config:base"] - ["config:recommended"] + ["config:best-practices"] | unique)`)
}

func (t CodeChange) pinContainerImageDigest() error {
	return jq(RENOVATE_JSON, `. + {"pinDigests":true}`)
}

func (t CodeChange) enableGoModTidy() error {
	if _, err := os.Stat("go.mod"); err != nil {
		return nil
	}

	return jq(RENOVATE_JSON, `.postUpdateOptions |= (. + ["gomodTidy"] | unique)`)
}

func (t CodeChange) enableGomodUpdateImportPaths() error {
	if _, err := os.Stat("go.mod"); err != nil {
		return nil
	}

	return jq(RENOVATE_JSON, `.postUpdateOptions |= (. + ["gomodUpdateImportPaths"] | unique)`)
}

func (t CodeChange) enableSubmodules() error {
	if _, err := os.Stat(".gitmodules"); err != nil {
		return nil
	}

	return jq(RENOVATE_JSON, `. + {"git-submodules":{"enabled": true}}`)
}

func (t CodeChange) disableHourlyPRLimit() error {
	return jq(RENOVATE_JSON, `. + {"prHourlyLimit":0}`)
}

func (t CodeChange) setConcurrentLimit() error {
	return jq(RENOVATE_JSON, `del(.prConcurrentLimit)`)
}

func (t CodeChange) deleteGitlabCIAutomerge() error {
	return jq(RENOVATE_JSON, `del(.gitLabAutomerge)`)
}

func (t CodeChange) setPlatformAutomerge() error {
	return jq(RENOVATE_JSON, `. + {"platformAutomerge": true}`)
}

func (t CodeChange) addRenovateLabel() error {
	return jq(RENOVATE_JSON, `. + {"labels":["renovate-bot"]}`)
}

func (t CodeChange) enableAutomerge() error {
	return jq(RENOVATE_JSON, `. + {"automerge":true, "automergeType": "pr"}`)
}

func (t CodeChange) disableDependencyDashboard() error {
	return jq(RENOVATE_JSON, `. + {"dependencyDashboard": false}`)
}

func (t CodeChange) kubernetesUpdates() error {
	templateDirectories, err := chartprettier.ListChartTemplateDirectories()
	if err != nil {
		return err
	}

	if len(templateDirectories) == 0 {
		return jq(RENOVATE_JSON, `del(.kubernetes)`)
	}

	for i, dir := range templateDirectories {
		templateDirectories[i] = "^" + dir + ".*\\.ya?ml(\\.tpl)?$"
	}

	json, err := json.Marshal(templateDirectories)
	if err != nil {
		return err
	}

	return jq(RENOVATE_JSON, fmt.Sprintf(`. + {"kubernetes": {"fileMatch": %s}}`, json))
}

func (t CodeChange) configurePostUpgradeTasks() error {
	commands, err := exocomp.ListPostUpgradeTasks()
	if err != nil {
		return err
	}

	err = jq(RENOVATE_JSON, `del(.postUpgradeTasks.fileFilter)`)
	if err != nil {
		return err
	}

	err = jq(RENOVATE_JSON, `. + {"postUpgradeTasks":{"commands":[], "fileFilters":["**/**"]}}`)
	if err != nil {
		return err
	}

	b, err := json.Marshal(commands)
	if err != nil {
		return err
	}

	err = jq(RENOVATE_JSON, fmt.Sprintf(`.postUpgradeTasks.commands |= (. + %s | unique)`, string(b)))
	if err != nil {
		return err
	}

	return nil
}

func jq(filename, action string) error {
	script := fmt.Sprintf(`cat <<< $(jq %s %s) > %s`, shellescape.Quote(action), shellescape.Quote(filename), shellescape.Quote(filename))

	cmd := exec.Command("bash")
	cmd.Stdin = strings.NewReader(script)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
