package gitlabci

import (
	"bytes"
	_ "embed"
	"fmt"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/utopia-planitia/test-infra/exocomp/codechanges/prettier"
	exocomp "gitlab.com/utopia-planitia/test-infra/exocomp/pkg"
	"gopkg.in/yaml.v3"
)

//go:embed gitlab-ci.yml.tpl
var gitlabci string

const (
	gitlabCIFile = ".gitlab-ci.yml"
)

var _ exocomp.CodeChange = (*CodeChange)(nil) // Verify that CodeChange implements exocomp.CodeChange.

type CodeChange struct{}

func (t CodeChange) New(_ *cli.Context) exocomp.Task {
	return t
}

func (t CodeChange) Enabled() bool {
	return true
}

func (t CodeChange) Batchable() bool {
	return true
}

func (t CodeChange) Name() string {
	return "gitlab-ci"
}

func (t CodeChange) Emoji() rune {
	return '🧪'
}

func (t CodeChange) Scope() exocomp.TaskScope {
	return exocomp.TaskScopeProject
}

func (t CodeChange) Flags() []cli.Flag {
	var flags []cli.Flag
	return flags
}

func (t CodeChange) MergeRequestTitle() string {
	return "configure gitlab ci"
}

func (t CodeChange) MergeRequestBody() string {
	return ""
}

func (t CodeChange) BranchName() string {
	return "exocomp/gitlab-ci"
}

func (t CodeChange) AutomergeGitlabCI() bool {
	return true
}

type CodeChangeTemplate struct {
	HelmfileImage     string
	GolangImage       string
	GolangCILintImage string
	UsesGolang        bool
	Helmfiles         []CodeChangeTemplateHelmfile
}

type CodeChangeTemplateHelmfile struct {
	Path            string
	UsesClusterYaml bool
}

func (t CodeChange) Apply(r exocomp.Repo, p *gitlab.Project, mr *gitlab.MergeRequest, c *cli.Context) (string, error) {
	data, err := restoredTemplateConfig()
	if err != nil {
		return "", fmt.Errorf("restore gitlab ci config: %v", err)
	}

	// service helmfiles
	helmfiles, err := r.ListHelmfiles()
	if err != nil {
		return "", fmt.Errorf("list helmfile files: %v", err)
	}

	for _, helmfile := range helmfiles {
		usesClusterYaml, err := usesClusterYaml(helmfile)
		if err != nil {
			return "", fmt.Errorf("identify cluster yaml usage: %v", err)
		}

		data.Helmfiles = append(data.Helmfiles, CodeChangeTemplateHelmfile{
			Path:            helmfile,
			UsesClusterYaml: usesClusterYaml,
		})
	}

	// Golang
	data.UsesGolang = true
	_, err = os.Stat("go.mod")
	if err != nil {
		if !os.IsNotExist(err) {
			return "", fmt.Errorf("identify go usage: %v", err)
		}

		data.UsesGolang = false
	}

	// remove
	managed := len(helmfiles) > 0 || data.UsesGolang

	if !managed {
		err := os.RemoveAll(gitlabCIFile)
		if err != nil {
			return "", fmt.Errorf("remove gitlab ci pipeline: %v", err)
		}

		err = prettier.UpdatePrettierignoreRemove(r, gitlabCIFile)
		if err != nil {
			return "", fmt.Errorf("remove gitlab ci from prettier ignore config: %v", err)
		}

		return "removed gitlab ci pipeline", nil
	}

	// create
	out, err := exocomp.RenderTemplate(gitlabci, data)
	if err != nil {
		return "", fmt.Errorf("parse templates: %v", err)
	}

	err = os.WriteFile(gitlabCIFile, out, 0o644)
	if err != nil {
		return "", fmt.Errorf("write gitlab pipeline: %v", err)
	}

	err = prettier.UpdatePrettierignoreAdd(r, gitlabCIFile)
	if err != nil {
		return "", fmt.Errorf("add gitlab ci to prettier ignore config: %v", err)
	}

	return "configured gitlab ci pipeline", nil
}

type GitlabCIPipeline struct {
	Image  string
	GoTest struct {
		Image string
	} `yaml:"go test"`
	GoLintTest struct {
		Image string
	} `yaml:"go lint"`
	HelmfileTest struct {
		Image string
	} `yaml:".helmfiletest"`
}

func restoredTemplateConfig() (CodeChangeTemplate, error) {
	current := CodeChangeTemplate{
		HelmfileImage:     "ghcr.io/utopia-planitia/helm-tools-image:latest",
		GolangImage:       "golang:1.17.3-buster",
		GolangCILintImage: "golangci/golangci-lint:v1.43.0-alpine",
		Helmfiles:         []CodeChangeTemplateHelmfile{},
	}

	gitlabCI, err := os.ReadFile(gitlabCIFile)
	if err != nil && os.IsNotExist(err) {
		return current, nil
	}
	if err != nil {
		return CodeChangeTemplate{}, err
	}

	data := GitlabCIPipeline{}

	err = yaml.Unmarshal(gitlabCI, &data)
	if err != nil {
		return CodeChangeTemplate{}, err
	}

	if data.HelmfileTest.Image != "" {
		current.HelmfileImage = data.HelmfileTest.Image
	}

	if data.Image != "" {
		current.HelmfileImage = data.Image
	}

	if data.GoTest.Image != "" {
		current.GolangImage = data.GoTest.Image
	}

	if data.GoLintTest.Image != "" {
		current.GolangCILintImage = data.GoLintTest.Image
	}

	if strings.HasPrefix(current.HelmfileImage, "utopiaplanitia/helm-tools:latest") {
		current.HelmfileImage = "ghcr.io/utopia-planitia/helm-tools-image:latest"
	}

	return current, nil
}

func usesClusterYaml(helmfile string) (bool, error) {
	content, err := os.ReadFile(helmfile)
	if err != nil {
		return false, err
	}

	found := bytes.Contains(content, []byte(".Values."))

	return found, nil
}
