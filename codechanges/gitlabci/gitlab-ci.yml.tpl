
default:
  interruptible: true
  timeout: 5m
{{ if .UsesGolang }}
go test:
  stage: test
  image: {{ .GolangImage }}
  script:
    - go test -v ./...

go lint:
  stage: test
  image: {{ .GolangCILintImage }}
  timeout: 5m
  script:
    - golangci-lint run ./...
{{ end }}{{ if .Helmfiles }}
.helmfiletest:
  stage: test
  image: {{ .HelmfileImage }}
{{ end }}
{{ range $helmfile := .Helmfiles -}}
{{ $helmfile.Path }} template:
  script:
    - helmfile --selector phase!=delete --file {{ $helmfile.Path }}{{ if $helmfile.UsesClusterYaml }} --state-values-file ../ci/cluster.yaml{{ end }} template
  extends: .helmfiletest

{{ $helmfile.Path }} lint:
  script:
    - helmfile --selector phase!=delete --file {{ $helmfile.Path }}{{ if $helmfile.UsesClusterYaml }} --state-values-file ../ci/cluster.yaml{{ end }} lint
  extends: .helmfiletest

{{ end -}}
